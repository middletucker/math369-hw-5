#!/bin/python3
'''
Times a function for various values of n
'''
import sys
import timeit
from typing import Callable, cast, Iterable, List, Tuple
from hw_5 import n_to_k_l


def print_table(rows: List[Tuple[int, int, float]]):
    n: int
    iterations: int
    total_time: float
    print('{:<26} {:<26} {:<26}'.format(
        'n', 'iterations', 'seconds/iteration'))
    print('-' * (26 * 3 + 2))
    for row in rows:
        n, iterations, total_time = row
        time_per_iter = total_time / iterations
        print('{:<26} {:<26} {:<26}'.format(n, iterations, time_per_iter))


def time_function_for_n(
        f: Callable[[int], Tuple[int, int]],
        n: int) -> Tuple[int, int, float]:
    timer: timeit.Timer = timeit.Timer(
        'f(n)', globals=locals()
    )
    loops, time = timer.autorange()
    return (n, loops, time)


def main() -> int:
    good_input: bool = False
    tries: int = 3
    while tries and not good_input:
        try:
            max_p: int = int(
                input('Enter a max p to time n=10^p [0+]: ')
            )
            if max_p < 0:
                raise ValueError
        except ValueError:
            print('p must be a positive integer or 0')
            tries -= 1
        else:
            print()
            good_input = True
    if not tries and not good_input:
        return 1
    else:
        rows = [time_function_for_n(n_to_k_l, 10**p)
                for p in
                cast(Iterable[int], range(0, max_p + 1))]
        print_table(rows)
        return 0


if __name__ == '__main__':
    sys.exit(main())
